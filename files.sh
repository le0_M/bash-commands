#!/bin/bash

# Create a directory and enter it.
mkdircd() {
    mkdir -p $1 && cd $1
}
