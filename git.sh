#!/bin/bash

# Stage all the track files and commit with the provided message.
gitac() {
    git add -A && git commit -m "$1"
}
